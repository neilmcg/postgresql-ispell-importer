package com.databasepatterns.postgres_ispell;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * @author Neil McGuigan
 * @since 5/12/2016
 */
public class CLI {

    private static final String urlBase = "https://src.chromium.org/svn/trunk/deps/third_party/hunspell_dictionaries/";

    public static void main(String[] args) throws Exception {

        String localeName = args[0];
        String localeURL = urlBase + localeName;
        String dicURL = localeURL + ".dic";
        String dicDeltaURL = localeURL + ".dic_delta";
        String affixURL = localeURL + ".aff";

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

        System.err.println("Requesting " + dicURL);
        String result = restTemplate.getForObject(dicURL, String.class);

        /* remove the UTF-8 Byte Order Mark (BOM) */
        if(result.startsWith("\uFEFF")) {
            result = result.substring(1);
        }

        String[] dicTemp = result.split("\n");

        String[] dic = Arrays.copyOfRange(dicTemp, 1, dicTemp.length); //remove the first element which is an id number

        System.err.println("Requesting " + dicDeltaURL);
        result = restTemplate.getForObject( dicDeltaURL, String.class );

        /* remove the UTF-8 BOM */
        if(result.startsWith("\uFEFF")) {
            result = result.substring(1);
        }

        String[] dicDelta = result.split("\n");

        String[] dict = concat(dic, dicDelta);
        System.err.println("Merged dictionaries");

        Arrays.sort(dict, String.CASE_INSENSITIVE_ORDER);
        System.err.println("Sorted dictionary");

        result = String.join("\n", dict);

        String filename = localeName.toLowerCase() + ".dict";
        Writer outputWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream( filename ), "UTF-8"));

        outputWriter.write( result );
        outputWriter.close();
        System.err.println("Wrote dictionary to " + filename);

        System.err.println("Requesting " + affixURL);
        result = restTemplate.getForObject(affixURL, String.class);

        filename = localeName.toLowerCase() + ".affix";
        outputWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "UTF-8"));
        outputWriter.write(result);
        outputWriter.close();
        System.err.println("Wrote affix file to " + filename);

        System.exit(0);
    }

    private static String[] concat(String[] a, String[] b) {
        int aLen = a.length;
        int bLen = b.length;
        String[] c= new String[aLen+bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }
}
