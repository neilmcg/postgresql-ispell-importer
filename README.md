Importing hunspell/ispell dictionaries is a bit of a pain in the butt.

This program eases the pain.

It downloads the dictionary and the delta dictionary, cleans, merges, sorts them, and outputs them to $local.dict in UTF-8 encoding. It also downloads and renames the affix file.

Compile with Maven:

`mvn package`

Run:

`java -jar target\postgres-ispell-jar-with-dependencies.jar en_CA`

This will write the following files in your working directory:

`en_ca.dict`
`en_ca.affix`

Copy these to the `share/tsearch_data` directory of your Postgres installation

Then in Postgres, run:

    create text search dictionary ispell_en_ca (
      template  =   ispell,
      dictfile  =   en_ca,
      afffile   =   en_ca,
      stopwords =   english
    );